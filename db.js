const mongoose = require('mongoose')
const page = require('./common/page')
const { DB_URL } = require('./config')

let dbOptions = {
  useNewUrlParser: true,
  useFindAndModify: false
}

function initialize (cb) {
  mongoose.set('useCreateIndex', true)
  mongoose.connect(DB_URL, dbOptions)
  let db = mongoose.connection
  page.create()
  db.on('error', console.error.bind(console, 'MONGOOSE CONNECTION ERROR'))
  db.once('open', function () {
    // page.init(cb)
    cb()
  })
}

module.exports = {
  initialize
}
