# base-server

Base HTTP server for REST API

# Includes

* JWT auth.
* Mongodb auth. ( You must create an user for database connection [```config.js```] )
* Mongoose implementation
* User CRUD API End-point
* Remember password [```config.js```]
