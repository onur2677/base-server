const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const rateLimit = require('express-rate-limit')
const bodyParser = require('body-parser')

const Response = require('./common/response')
const { HTTP_PORT } = require('./config')
const logger = require('./common/logger')
const db = require('./db')

const limiter = rateLimit({
  windowMs: 1 * 60 * 1000,
  max: 50,
  message: {
    success: false,
    data: null,
    errorKey: 'REQUEST_LIMIT'
  }
})

const app = express()
const v1 = require('./api/v1/v1')

app.use(bodyParser.json())
app.use(limiter)
app.use(helmet())
app.use(cors())
app.disable('x-powered-by')

app.use('/v1', v1)

app.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    const response = new Response(false, null, err.name)
    res.status(401).send(response)
  } else {
    next(err)
  }
})

db.initialize(() => {
  app.listen(HTTP_PORT, () => logger.log(`Server listening on port ${HTTP_PORT}!`))
})
