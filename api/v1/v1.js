const express = require('express')
const router = express.Router()
const exjwt = require('express-jwt')

const user = require('./routes/user/user')
const role = require('./routes/role/role')

const { JWT_KEY } = require('../../config')

const jwtMW = exjwt({
  secret: JWT_KEY
})

// router.use('/example-route', jwtMW, route)
router.use('/user', jwtMW.unless({ path: ['/v1/user/login', '/v1/user/remember-password'] }), user)
router.use('/role', jwtMW, role)
module.exports = router
