const mongoose = require('mongoose')
const logger = require('../../../common/logger')
const ERROR = require('../../../common/error')
class BaseModel {
  constructor (schemaName, schemaObject) {
    this.Model = mongoose.model(schemaName, schemaObject)
  }

  find (data, cb) {
    let query = this.Model.findOne(data.params)
    if (data.populates) {
      query.populate(data.populates)
    }
    query.exec((err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }

      if (data) {
        logger.info(data)
        return cb(null, data)
      }

      return cb(new Error(ERROR.FIND_ERR), null)
    })
  }

  findAll (data, cb) {
    let query = this.Model.find(data.params)
    if (data.populates) {
      query.populate(data.populates)
    }
    query.exec((err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }

      if (data) {
        logger.info(data)
        return cb(null, data)
      }

      return cb(new Error(ERROR.FIND_ERR), null)
    })
  }

  save (data, cb) {
    data.params._id = mongoose.Types.ObjectId()
    const item = new this.Model(data.params)
    item.save((err) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }
      logger.info(data.params)
      return cb(null, data.params)
    })
  }

  update (data, cb) {
    this.Model.findByIdAndUpdate(data.params.id, { $set: data.params }, { new: true }).exec((err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }
      if (data) {
        logger.info(data)
        return cb(null, data)
      }

      return cb(new Error(ERROR.UPDATE_ERR), null)
    })
  }

  delete (data, cb) {
    /*     this.Model.findByIdAndDelete(data.params.id).exec((err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }
      if (data) {
        logger.info(data)
        return cb(null, data)
      }

      return cb(new Error(ERROR.DELETE_ERR), null)
    }) */
    this.Model.deleteOne({ _id: data.params.id }).exec((err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }
      if (data) {
        logger.info(data)
        return cb(null, data)
      }

      return cb(new Error(ERROR.DELETE_ERR), null)
    })
  }
}

module.exports = BaseModel
