const Response = require('../../../common/response')
function requestHandler (req, res) {
  const operations = {
    POST: 'save',
    PUT: 'update',
    DELETE: 'delete',
    GET: req.params._id ? 'find' : 'findAll'
  }
  const operation = operations[req.method]
  const data = {
    params: Object.assign({}, req.body, req.params),
    populates: this.populates
  }
  this.Model[operation](data, (error, data) => {
    let success = false
    let responseCode = 500
    let responseData = null
    let errorKey = ''

    if (error) {
      errorKey = error.message || error.name
    } else {
      responseData = data
      responseCode = 200
      success = true
    }

    const response = new Response(success, responseData, errorKey)
    res.status(responseCode).send(response)
  })
}

module.exports = requestHandler
