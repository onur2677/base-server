const router = require('express').Router()
const { ObjectId } = require('mongoose').Types
const Model = require('./modules/role-model')
const requestHandler = require('../../modules/request-handler-middleware')

const populates = { path: 'pages.page' }

router.get('/', requestHandler.bind({ Model, populates }))
router.get('/:_id', requestHandler.bind({ Model, populates }))
router.post('/', (req, res) => {
  for (let page of req.body.pages) {
    page._id = ObjectId(page._id)
  }
  requestHandler.bind({ Model })(req, res)
})
router.put('/:id', requestHandler.bind({ Model }))
router.delete('/:id', requestHandler.bind({ Model }))

module.exports = router
