
const BaseModel = require('../../../modules/base-model')
const schema = require('./role-schema')

class Role extends BaseModel {
  constructor (schemaName, schemaObject) {
    super(schemaName, schemaObject)
  }
}

module.exports = new Role('Roles', schema)
