const { Schema } = require('mongoose')
const userModel = require('../../user/modules/user-model')
const role = new Schema({
  _id: Schema.Types.ObjectId,
  name: {
    type: Schema.Types.String,
    required: true,
    unique: true
  },
  pages: [{
    page: { type: Schema.Types.ObjectId, ref: 'Pages' },
    operation: { type: Schema.Types.String, enum: ['view', 'modify', 'full'] }
  }]
})

role.pre('deleteOne', function () {
  let { _id } = this.getQuery()
  userModel.findAll({ params: { roles: _id } }, (err, data) => {
    if (!err) {
      for (let user of data) {
        const index = user.roles.findIndex(item => item.toString() === _id)
        user.roles.splice(index, 1)
        user.save(err => console.log(err))
      }
    }
  })
})

module.exports = role
