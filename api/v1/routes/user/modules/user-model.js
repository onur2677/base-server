
const BaseModel = require('../../../modules/base-model')
const logger = require('../../../../../common/logger')
const ERROR = require('../../../../../common/error')
const mailer = require('../../../../../common/mailer')
const schema = require('./user-schema')

class User extends BaseModel {
  constructor (schemaName, schemaObject) {
    super(schemaName, schemaObject)
  }

  passwordRemember (email, cb) {
    this.find({ email }, (err, data) => {
      if (err) {
        logger.error(err.message)
        return cb(err, null)
      }

      if (data) {
        return mailer.send({
          from: 'Nativesoft Bilişim Hizmetleri',
          to: email,
          subject: 'Parola hatırlatma',
          text: '',
          html: `<p>Kullanıcı adınız : ${data.username}</p><p>Şifreniz :${data.password}</p>`
        }, cb)
      }

      return cb(new Error(ERROR.FIND_ERR), null)
    })
  }
}

module.exports = new User('Users', schema)
