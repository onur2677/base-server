const { Schema } = require('mongoose')
const user = new Schema({
  _id: Schema.Types.ObjectId,
  username: {
    type: Schema.Types.String,
    required: true,
    unique: true
  },
  name: {
    type: Schema.Types.String,
    required: true
  },
  surname: {
    type: Schema.Types.String,
    required: true
  },
  password: {
    type: Schema.Types.String,
    required: true
  },
  email: {
    type: Schema.Types.String,
    required: true,
    unique: true
  },
  date: {
    type: Schema.Types.Date,
    default: Date.now
  },
  mobile: {
    type: Schema.Types.String,
    unique: true
  },
  roles: [{
    type: Schema.Types.ObjectId,
    ref: 'Roles'
  }]
})

module.exports = user
