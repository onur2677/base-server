const router = require('express').Router()
const { ObjectId } = require('mongoose').Types
const jwt = require('jsonwebtoken')
const Model = require('./modules/user-model')
const Response = require('../../../../common/response')
const { JWT_KEY, JWT_EXPIRE } = require('../../../../config')
const requestHandler = require('../../modules/request-handler-middleware')

router.post('/login', (req, res) => {
  Model.find({ params: req.body }, (error, data) => {
    let success = false
    let responseCode = 401
    let responseData = null
    let errorKey = ''

    if (error) {
      errorKey = error.message || error.name
    } else {
      let token = jwt.sign({ id: data._id, username: data.username }, JWT_KEY, { expiresIn: JWT_EXPIRE }) // Sigining the token
      responseData = {
        token,
        _id: data._id
      }
      responseCode = 200
      success = true
    }

    const response = new Response(success, responseData, errorKey)
    res.status(responseCode).send(response)
  })
})

router.post('/remember-password', (req, res) => {
  Model.passwordRemember(req.body.email, (error, data) => {
    let success = false
    let responseCode = 401
    let responseData = null
    let errorKey = ''

    if (error) {
      errorKey = error.message || error.name
    } else {
      responseData = data
      responseCode = 200
      success = true
    }

    const response = new Response(success, responseData, errorKey)
    res.status(responseCode).send(response)
  })
})

const populates = { path: 'users._id' }

router.get('/', requestHandler.bind({ Model, populates }))
router.get('/:_id', requestHandler.bind({ Model, populates }))

router.post('/', (req, res) => {
  if (req.body.roles) {
    for (let role of req.body.roles) {
      role._id = ObjectId(role._id)
    }
  }
  requestHandler.bind({ Model })(req, res)
})
router.put('/:id', requestHandler.bind({ Model }))
router.delete('/:id', requestHandler.bind({ Model }))

module.exports = router
