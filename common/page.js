const { Schema, model, Types } = require('mongoose')
const PAGES = [
  {
    _id: Types.ObjectId(),
    key: 'homepage'
  },
  {
    _id: Types.ObjectId(),
    key: 'users'
  }
]

const page = new Schema({
  _id: Schema.Types.ObjectId,
  key: {
    type: Schema.Types.String,
    required: true,
    unique: true
  }
})

module.exports = {
  create: () => {
    page.statics.init = (cb) => {
      Page.find({}, (err, data) => {
        if (err) {
          throw err
        }
        if (data.length === 0) {
          Page.create(PAGES)
          console.log('PAGES INITIALIZED')
        }
        cb()
      })
    }

    const Page = model('Pages', page)
  }
}
