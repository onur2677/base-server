const { SEND_MAIL } = require('../config')
const nodeMailer = require('nodemailer')
class Mailer {
  constructor (email, password) {
    this.transporter = nodeMailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      auth: {
        user: email,
        pass: password
      }
    })
  }

  /**
   *
   * @param {object} mailOptions
   * @param {string} mailOptions.from
   * @param {string} mailOptions.to
   * @param {string} mailOptions.subject
   * @param {string} mailOptions.text
   * @param {string} mailOptions.html
   */
  send (mailOptions, cb) {
    this.transporter.sendMail(mailOptions, cb)
  }
}

module.exports = new Mailer(SEND_MAIL.EMAIL, SEND_MAIL.PASSWORD)
