/**
 * @param {Boolean} success
 * @param {*} data
 * @param {String} errorKey
 */
class Response {
  constructor(success, data, errorKey) {
    this.success = success
    this.data = data
    this.errorKey = errorKey
  }
}
module.exports = Response
